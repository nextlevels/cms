<?php namespace NextLevels\Cms\Classes;

use Cms\Classes\CmsException;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Lang;
use NextLevels\Cms\Models\Page as CMSPage;

class PageHandler
{
    use \October\Rain\Support\Traits\Singleton;

    protected $theme;

    /**
     * Initialize this singleton.
     */
    protected function init()
    {
        $this->theme = Theme::getActiveTheme();
        if (!$this->theme) {
            throw new CmsException(Lang::get('cms::lang.theme.active.not_found'));
        }
    }

    public function getPage($url)
    {
        $cmsPage = CMSPage::where('slug',$url)->first();

        if (!$cmsPage) {
            return null;
        }


        $systemPage = Page::inTheme($this->theme);
        $systemPage->url = $url;
        $systemPage->title = $cmsPage->name;

        $systemPage->layout = 'default';
        $systemPage->code = 'test ';
        $systemPage->markup = 'test ';
        $systemPage->apiBag['pageData'] = $cmsPage;


        // Transer page ID to CMS page
        $systemPage->settings['id'] = $cmsPage->id;

        return $systemPage;
    }

    public function injectPageTwig($page, $loader, $twig)
    {
        if (!isset($page->apiBag['pageData'])) {
            return;
        }

        $staticPage = $page->apiBag['pageData'];

    }

    public function getPageContents($page)
    {
        if (!isset($page->apiBag['pageData'])) {
            return;
        }

        return $page->apiBag['pageData']->renderHTML();
    }


}
