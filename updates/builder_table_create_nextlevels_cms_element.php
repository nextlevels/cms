<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNextlevelsCmsElement extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_cms_element', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->boolean('active')->default(1);
            $table->text('content');
            $table->integer('parent')->nullable();
            $table->integer('sort')->default(1);
            $table->integer('element_template_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nextlevels_cms_element');
    }
}
