<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNextlevelsCmsLayout extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_cms_layout', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nextlevels_cms_layout');
    }
}
