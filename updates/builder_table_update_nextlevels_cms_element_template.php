<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsCmsElementTemplate extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_cms_element_template', function($table)
        {
            $table->renameColumn('fields', 'content');
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_cms_element_template', function($table)
        {
            $table->renameColumn('content', 'fields');
        });
    }
}
