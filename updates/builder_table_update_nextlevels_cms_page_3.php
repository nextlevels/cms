<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsCmsPage3 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_cms_page', function($table)
        {
            $table->renameColumn('layout', 'layout_id');
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_cms_page', function($table)
        {
            $table->renameColumn('layout_id', 'layout');
        });
    }
}
