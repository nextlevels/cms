<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsCmsPage4 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_cms_page', function($table)
        {
            $table->boolean('active')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_cms_page', function($table)
        {
            $table->boolean('active')->default(null)->change();
        });
    }
}
