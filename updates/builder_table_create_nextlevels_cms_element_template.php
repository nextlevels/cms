<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNextlevelsCmsElementTemplate extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_cms_element_template', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->text('fields');
            $table->text('markup');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nextlevels_cms_element_template');
    }
}
