<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsCmsElement3 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_cms_element', function($table)
        {
            $table->renameColumn('element_template_id', 'template_id');
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_cms_element', function($table)
        {
            $table->renameColumn('template_id', 'element_template_id');
        });
    }
}
