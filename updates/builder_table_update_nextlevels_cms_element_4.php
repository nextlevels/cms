<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsCmsElement4 extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_cms_element', function($table)
        {
            $table->text('content')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_cms_element', function($table)
        {
            $table->text('content')->nullable(false)->change();
        });
    }
}
