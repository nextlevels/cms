<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsCmsPage extends Migration
{
    public function up()
    {
        Schema::rename('nextlevels_cms_pages', 'nextlevels_cms_page');
    }
    
    public function down()
    {
        Schema::rename('nextlevels_cms_page', 'nextlevels_cms_pages');
    }
}
