<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNextlevelsCmsPages extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_cms_pages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 191);
            $table->string('slug', 191);
            $table->integer('layout');
            $table->boolean('active');
            $table->string('seo_title', 191)->nullable();
            $table->text('seo_description')->nullable();
            $table->string('seo_keywords')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nextlevels_cms_pages');
    }
}
