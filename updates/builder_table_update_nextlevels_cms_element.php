<?php namespace NextLevels\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateNextlevelsCmsElement extends Migration
{
    public function up()
    {
        Schema::table('nextlevels_cms_element', function($table)
        {
            $table->integer('page_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('nextlevels_cms_element', function($table)
        {
            $table->dropColumn('page_id');
        });
    }
}
