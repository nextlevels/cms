<?php namespace NextLevels\Cms\Models;

use Model;

/**
 * Model
 */
class ElementTemplate extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_cms_element_template';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array Guarded fields
     */
    protected $jsonable = ['content'];
}
