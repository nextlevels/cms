<?php namespace NextLevels\Cms\Models;

use Model;

/**
 * Model
 */
class Page extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_cms_page';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = ['layout' => [Layout::class]];

    public $hasMany = ['elements' => Element::class];

    public function renderHTML(){
        $html = '';
            foreach ($this->elements()->get() as $value){
                $html .= $value->renderHTML();
            }
        return $html;
    }
}
