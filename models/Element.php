<?php namespace NextLevels\Cms\Models;

use Model;

/**
 * Model
 */
class Element extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_cms_element';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array Guarded fields
     */
    protected $jsonable = ['content'];

    public $belongsTo = ['page' => [Page::class],'template' =>  [ElementTemplate::class] ];

    public function renderHTML(){
        $markup = $this->template()->first()->markup;
         foreach ($this->content as $value){
             $markup = str_replace('{'.$value['name'].'}', $value['Art'], $markup);
         }
         return $markup;
    }
}
