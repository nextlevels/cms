<?php namespace NextLevels\Cms\Observers;

use Exception;
use NextLevels\Cms\Models\Element;

class ElementObserver
{
    /**
     * After create element
     *
     * @param Element $element
     *
     * @throws Exception
     */
    public function creating(Element $element): void
    {
        $element->content = $element->template->content;

    }
}
