<?php namespace NextLevels\Cms\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use NextLevels\Cms\Models\ElementTemplate;
use October\Rain\Database\Collection;

class PagesController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController' ,        'Backend\Behaviors\RelationController'   ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('NextLevels.Cms', 'next-cms-main', 'next-cms-main-pages');
    }

    /**
     * @return mixed
     */
    public function getTemplateFields(): Collection
    {
        return ElementTemplate::all();
    }

    /**
     * @return mixed
     */
    public function getPageElements(): Collection
    {
        return $this->formGetModel()->elements()->with('template')->get();
    }


}
