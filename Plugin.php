<?php namespace NextLevels\Cms;

use Cms\Classes\Controller as CmsController;
use Event;
use NextLevels\Cms\Classes\PageHandler;
use NextLevels\Cms\Models\Element;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        Element::observe(Observers\ElementObserver::class);

        Event::listen('cms.router.beforeRoute', function($url) {
            return PageHandler::instance()->getPage($url);
        });

        Event::listen('cms.page.beforeRenderPage', function($controller, $page) {
            /*
             * Before twig renders
             */
            $twig = $controller->getTwig();
            $loader = $controller->getLoader();
            PageHandler::instance()->injectPageTwig($page, $loader, $twig);

            /*
             * Get rendered content
             */
            $contents = PageHandler::instance()->getPageContents($page);
            if (strlen($contents)) {
                return $contents;
            }
        });

        Event::listen('cms.page.initComponents', function($controller, $page) {

        });


    }
}
